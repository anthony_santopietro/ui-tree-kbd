(function (angular) {
    'use strict';

    /**
     * @ngdoc overview
     * @name ui.tree-keyboard
     */
    angular.module('ui.tree-keyboard', [
        'ui.tree',
        'ui.keypress',
    ])
    /**
     * @ngdoc object
     * @name ui.tree-keyboard.value:uiTreeKeyboardConfig
     *
     * @description
     * Provides module configuration. Event (keyup, keydown), event namespace and scope property name can be customized
     * here.
     */
        .value('uiTreeKeyboardConfig', {
            // FIXME: using `keyup` does not prevent page from scrolling.
            eventName: 'keydown',
            pointerPropertyName: 'uiTreeKeyboardPointer',
            optionsPropertyName: 'uiTreeKeyboardOptions',
            confirmationPromptText: 'Node has children. Are you sure?',
            customMapping: {},
            setPointerOnFirst: true,
            dragPointerToNewNode: true,
        })
    /**
     * @ngdoc object
     * @name ui.tree-keyboard.provider:uiTreeApiFacade
     *
     * @description
     * Provides configurable higher level API to interact with the tree. Groups helper methods focusing on tree node
     * operations like creating, deleting, traversal, switching binary flags etc.
     *
     * @todo: extend description for uiTreeApiFacadeProvider
     */
        .provider('uiTreeApiFacade', function uiTreeApiFacadeProvider() {
            const customMethodDefinitions = [];

            /**
             * Allows to add own helpers or modify existing ones.
             *
             * @param {object} object
             */
            this.registerMethods = function (object) {
                customMethodDefinitions.push(object);
            };

            /**
             * Creates `uiTreeApiFacade` service.
             * @returns {object}
             */
            this.$get = function () {

                /**
                 * @type {{
                 *     addChild: addChild,
                 *     addSibling: addSibling,
                 *     remove: remove,
                 *     hasParentNode: hasParentNode,
                 *     hasChildNodes: hasChildNodes,
                 *     hasPreviousSibling: hasPreviousSibling,
                 *     hasNextSibling: hasNextSibling,
                 *     isCollapsed: isCollapsed,
                 *     getParentScope: getParentScope,
                 *     getPreviousSiblingScope: getPreviousSiblingScope,
                 *     getNextSiblingScope: getNextSiblingScope,
                 *     getLastChildOf: getLastChildOf,
                 *     getFirstChildOf: getFirstChildOf,
                 *     toggle: toggle,
                 * }}
                 */
                const methods = {
                    /**
                     * Creates a new node in given.
                     *
                     * @param {object} $scope
                     */
                    addChild: function addChild($scope) {
                        $scope.initSubNode({});
                    },
                    /**
                     * Creates a new below given.
                     *
                     * @param {object} $scope
                     */
                    addSibling: function addSibling($scope) {
                        $scope.initSubNode();
                    },
                    /**
                     * Removes a node with its children.
                     *
                     * @param {object} $scope
                     */
                    remove: function remove($scope) {
                        $scope.remove();
                    },
                    /**
                     * Checks if scope has parent node.
                     *
                     * @param {object} $scope
                     * @returns {boolean}
                     */
                    hasParentNode: function hasParentNode($scope) {
                        return $scope.depth() > 1;
                    },
                    /**
                     * Checks if scope has child nodes.
                     *
                     * @param {object} $scope
                     * @returns {boolean}
                     */
                    hasChildNodes: function hasChildNodes($scope) {
                        return $scope.hasChild();
                    },
                    /**
                     * Checks if scope has previous sibling
                     *
                     * @param {object} $scope
                     * @returns {boolean}
                     */
                    hasPreviousSibling: function hasPreviousSibling($scope) {
                        return $scope.index() > 0;
                    },
                    /**
                     * Checks if scope has next sibling
                     *
                     * @param {object} $scope
                     * @returns {boolean}
                     */
                    hasNextSibling: function hasNextSibling($scope) {
                        return $scope.siblings().length > $scope.index() + 1;
                    },
                    /**
                     * Checks if node is collapsed.
                     *
                     * @param {object} $scope
                     * @returns {boolean}
                     */
                    isCollapsed: function isCollapsed($scope) {
                        return !!$scope.collapsed;
                    },
                    /**
                     * Extracts parent node scope.
                     *
                     * @param {object} $scope
                     * @returns {Object|undefined} a scope
                     */
                    getParentScope: function getParentScope($scope) {
                        if (this.hasParentNode($scope)) {
                            return $scope.$parentNodesScope.$parent;
                        }
                    },
                    /**
                     * Accesses previous sibling scope.
                     *
                     * @param {object} $scope
                     * @returns {Object|undefined} a scope
                     */
                    getPreviousSiblingScope: function getPreviousSiblingScope($scope) {
                        return $scope.prev();
                    },
                    /**
                     * Accesses next sibling scope.
                     *
                     * @param {object} $scope
                     * @returns {Object|undefined} a scope
                     */
                    getNextSiblingScope: function getNextSiblingScope($scope) {
                        return $scope.siblings()[$scope.index() + 1];
                    },
                    /**
                     * Accesses last child scope.
                     *
                     * @param {object} $scope
                     * @returns {Object|undefined} a scope
                     */
                    getLastChildOf: function getLastChildOf($scope) {
                        const childNodes = $scope.childNodes();
                        return childNodes ? childNodes[$scope.childNodesCount() - 1] : undefined;
                    },
                    /**
                     * Accesses first child of scope.
                     *
                     * @param {object} $scope
                     * @returns {Object|undefined} a scope
                     */
                    getFirstChildOf: function getLastChildOf($scope) {
                        const childNodes = $scope.childNodes();
                        return childNodes ? childNodes[0] : undefined;
                    },
                    /**
                     * Toggles node children visibility.
                     *
                     * @param {object} $scope
                     */
                    toggle: function toggle($scope) {
                        $scope.toggle();
                    },
                };

                customMethodDefinitions.forEach(function (definition) {
                    angular.extend(methods, definition);
                });

                return methods;
            };
        })
    /**
     * @ngdoc object
     * @name ui.tree-keyboard.provider:uiTreeKeyboardActions
     *
     * @description
     * @todo: description for uiTreeKeyboardActionsProvider
     *
     * @requires ng.$document
     * @requires ng.$timeout
     * @requires ng.$window
     * @requires ui.tree.service:$helper
     */
        .provider('uiTreeKeyboardActions', function uiTreeKeyboardActionsProvider() {
            const customActionsDefinitions = [];

            /**
             * Allows to add own actions or modify existing ones before app starts.
             *
             * @param {object} object
             */
            this.registerActions = function (object) {
                customActionsDefinitions.push(object);
            };

            /**
             *
             * @param {object} $document
             * @param {object} $timeout
             * @param {object} $window
             * @param {object} uiTreeApiFacade
             * @param {object} uiTreeKeyboardConfig
             * @returns {{
             *      create: {},
             *      remove: {},
             *      smartMove: {},
             *      move: {},
             *      smartNavigateTo: {},
             *      navigateTo: {},
             *      toggle: {}
             *  }}
             */
            this.$get = function ($document, $timeout, $window, uiTreeApiFacade, uiTreeKeyboardConfig) {
                /**
                 * Polyfill for ES6 Rest Parameters
                 * @param {Array} [argArray]
                 * @returns {Array}
                 */
                function rest(argArray) {
                    return [].slice.call(argArray).slice(1);
                }

                /**
                 * Switches first argument to given.
                 * @param {object} scope
                 * @param {Array} [argArray]
                 * @returns {Array}
                 */
                function withScope(scope, argArray) {
                    return [scope].concat(rest(argArray));
                }

                /**
                 * Transfers pointer to other scope according to the following logic:
                 * 1) If given scope has sibling go to previous sibling
                 * 2) If it doesn't: choose parent if exists
                 * 3) If it doesn't: choose next sibling
                 * Or do nothing.
                 * Used as helper for node removal;
                 *
                 * @param {object} scope
                 * @param {object} options
                 * @param options.$event        keyboard event object
                 * @param options.movePointerTo pointer callback
                 *
                 * @todo: this method might be useful as a public method.
                 */
                function transferPointerAfterNodeRemoval(scope, options) {
                    const previousSiblingScope = uiTreeApiFacade.getPreviousSiblingScope(scope);
                    const parentScope = uiTreeApiFacade.getParentScope(scope);
                    const nextSiblingScope = uiTreeApiFacade.getNextSiblingScope(scope);

                    if (previousSiblingScope) {
                        options.movePointerTo(previousSiblingScope);
                    } else if (parentScope) {
                        options.movePointerTo(parentScope);
                    } else if (nextSiblingScope) {
                        options.movePointerTo(nextSiblingScope);
                    } else {
                        console.log('you have just deleted the last element. no parent, no siblings ;)');
                    }
                }

                /**
                 * All actions take 2 arguments:
                 * - scope {object}
                 * - options {{$event: object, movePointerTo: {function}}}
                 *
                 * Possible usage of options:
                 * - options.$event.preventDefault();
                 * - options.movePointerTo(scope);
                 * - options.movePointerTo(anyOtherScope);
                 */
                const actions = {
                    create: {
                        child: function (scope, options) {
                            uiTreeApiFacade.addChild.apply(this, arguments);
                            if (uiTreeKeyboardConfig.dragPointerToNewNode) {
                                // TODO: make it a promise!
                                // Add child should return a promise and when this promise is resolved pointer should
                                // be moved
                                $timeout(function () {
                                    options.movePointerTo(uiTreeApiFacade.getNextSiblingScope(scope), true);
                                }, 100);
                            }
                        },
                        sibling: function (scope, options) {
                            uiTreeApiFacade.addSibling.apply(this, arguments);
                            console.log(uiTreeKeyboardConfig.dragPointerToNewNode);
                            if (uiTreeKeyboardConfig.dragPointerToNewNode) {
                                // TODO: make it a promise!
                                // Add child should return a promise and when this promise is resolved pointer should
                                // be moved
                                $timeout(function () {
                                    options.movePointerTo(uiTreeApiFacade.getNextSiblingScope(scope), true);
                                }, 100);
                            }
                        },
                    },
                    remove: {
                        confirmed: function (scope) {
                            if (uiTreeApiFacade.hasChildNodes(scope)) {
                                if ($window.confirm(uiTreeKeyboardConfig.confirmationPromptText)) {
                                    actions.remove.force.apply(this, arguments);
                                }
                            } else {
                                actions.remove.force.apply(this, arguments);
                            }
                        },
                        force: function (scope) {
                            transferPointerAfterNodeRemoval.apply(this, arguments);
                            uiTreeApiFacade.remove(scope);
                        },
                    },
                    smartMove: {
                        up: function (scope, options) {
                            if (scope.$first) {
                                options.$event.preventDefault();
                                options.asPreviousSibling = true;
                                actions.smartMove.$asSiblingOfParent(scope, options);
                            } else if (uiTreeApiFacade.hasChildNodes(uiTreeApiFacade.getPreviousSiblingScope(scope))) {
                                options.$event.preventDefault();
                                actions.smartMove.$asLastDescendantOfPreviousSibling.apply(this, arguments);
                            } else {
                                actions.move.up.apply(this, arguments);
                            }
                        },
                        down: function (scope, options) {
                            if (scope.$last) {
                                actions.move.out.apply(this, arguments);
                            } else if (uiTreeApiFacade.hasChildNodes(uiTreeApiFacade.getNextSiblingScope(scope))) {
                                options.$event.preventDefault();
                                actions.smartMove.$asFirstChildOfNextSibling.apply(this, arguments);
                            } else {
                                actions.move.down.apply(this, arguments);
                            }
                        },
                        $asLastDescendantOfPreviousSibling: function (scope, options) {
                            const previousSibling = uiTreeApiFacade.getPreviousSiblingScope(scope);

                            previousSibling.$childNodesScope
                                .insertNode(previousSibling.childNodesCount(), scope.remove().$modelValue);

                            // FIXME: there's a race condition somewhere around here. Scope is initialised (and
                            // therefore the `$id` is available) after the element is rendered.
                            $timeout(function () {
                                options.movePointerTo(uiTreeApiFacade.getLastChildOf(previousSibling));
                            }, 100);
                        },
                        $asFirstChildOfNextSibling: function (scope, options) {
                            const nextSibling = uiTreeApiFacade.getNextSiblingScope(scope);
                            nextSibling.$childNodesScope
                                .insertNode(0, scope.remove().$modelValue);


                            $timeout(function () {
                                options.movePointerTo(uiTreeApiFacade.getFirstChildOf(nextSibling));
                            }, 100); //FIXME: there's a race condition here. Maybe `insertNode` should return a promise.
                        },
                        $asSiblingOfParent: function (scope, options) {
                            if (!uiTreeApiFacade.hasParentNode(scope)) {
                                return;
                            }

                            const parent = scope.$parentNodesScope;
                            const position = parent.index() + (options.asPreviousSibling ? 0 : 1);

                            parent.$parentNodesScope.insertNode(position, scope.remove().$modelValue);

                            $timeout(function () {
                                options.movePointerTo(parent.siblings()[position], true);
                            }, 100); //FIXME: there's a race condition here. Maybe `insertNode` should return a promise.
                        },
                    },
                    move: {
                        up: function (scope, options) {
                            options.$event.preventDefault();

                            if (scope.$first) {
                                return;
                            }

                            const previousSibling = uiTreeApiFacade.getPreviousSiblingScope(scope);
                            scope.$parentNodesScope.insertNode(previousSibling.index(), scope.remove().$modelValue);
                        },
                        down: function (scope, options) {
                            options.$event.preventDefault();

                            if (scope.$last) {
                                return;
                            }

                            const nextSibling = uiTreeApiFacade.getNextSiblingScope(scope);
                            scope.$parentNodesScope.insertNode(nextSibling.index(), scope.remove().$modelValue);
                        },
                        'in': function (scope, options) {
                            options.$event.preventDefault();

                            if (!uiTreeApiFacade.hasPreviousSibling(scope)) {
                                return;
                            }

                            const previousSibling = uiTreeApiFacade.getPreviousSiblingScope(scope);

                            previousSibling.$childNodesScope
                                .insertNode(previousSibling.childNodesCount(), scope.remove().$modelValue);

                            $timeout(function () {
                                options.movePointerTo(uiTreeApiFacade.getLastChildOf(previousSibling), true);
                            }, 100); //FIXME: there's a race condition here. Maybe `insertNode` should return a promise.
                        },
                        out: function (scope, options) {
                            options.$event.preventDefault();
                            actions.smartMove.$asSiblingOfParent.apply(this, arguments);
                        },
                        top: function moveToTop(scope, options) {
                            options.$event.preventDefault();

                            if (scope.$first) {
                                return;
                            }

                            scope.$parentNodesScope.insertNode(0, scope.remove().$modelValue);
                        },
                        bottom: function moveToBottom(scope, options) {
                            options.$event.preventDefault();

                            if (scope.$last) {
                                return;
                            }

                            scope.$parentNodesScope.insertNode(scope.siblings().length - 1, scope.remove().$modelValue);
                        },
                    },
                    smartNavigateTo: {
                        left: function smartNavigateToLeft(scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasChildNodes(scope)) {
                                return (uiTreeApiFacade.isCollapsed(scope) ?
                                    actions.smartNavigateTo.previous :
                                    actions.toggle.collapsed).apply(this, arguments);
                            } else {
                                actions.smartNavigateTo.previous.apply(this, arguments);
                            }
                        },
                        right: function smartNavigateToRight(scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasChildNodes(scope)) {
                                return (uiTreeApiFacade.isCollapsed(scope) ?
                                    actions.toggle.collapsed :
                                    actions.smartNavigateTo.next).apply(this, arguments);
                            } else {
                                actions.smartNavigateTo.next.apply(this, arguments);
                            }
                        },
                        previous: function smartNavigateToPreviousNode(scope, options) {
                            options.$event.preventDefault();

                            const previousSiblingScope = uiTreeApiFacade.getPreviousSiblingScope(scope);

                            if (scope.$first && uiTreeApiFacade.hasParentNode(scope)) {
                                actions.navigateTo.parent.apply(this, arguments);
                            } else if (previousSiblingScope && !uiTreeApiFacade.isCollapsed(previousSiblingScope) &&
                                uiTreeApiFacade.hasChildNodes(previousSiblingScope)) {
                                actions.smartNavigateTo.lastVisibleDescendant
                                    .apply(this, withScope(previousSiblingScope, arguments));
                            } else {
                                actions.navigateTo.previous.apply(this, arguments);
                            }
                        },
                        next: function smartNavigateToNextNode(scope, options) {
                            options.$event.preventDefault();

                            function isVisuallyLast(scope) {
                                function childrenCollapsed(scope) {
                                    return uiTreeApiFacade.hasChildNodes(scope) && uiTreeApiFacade.isCollapsed(scope);
                                }

                                return (scope.$last && !uiTreeApiFacade.hasChildNodes(scope)) ||
                                    childrenCollapsed(scope);
                            }

                            if (isVisuallyLast(scope)) {
                                actions.smartNavigateTo.nearestAncestorNextSibling.apply(this, arguments);
                            } else if (uiTreeApiFacade.hasChildNodes(scope) && !uiTreeApiFacade.isCollapsed(scope)) {
                                actions.navigateTo.child.apply(this, arguments);
                            } else {
                                actions.navigateTo.next.apply(this, arguments);
                            }
                        },
                        nearestAncestorNextSibling: function (scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasNextSibling(scope)) {
                                actions.navigateTo.next.apply(this, withScope(scope, arguments));
                            } else {
                                actions.smartNavigateTo.nearestAncestorNextSibling
                                    .apply(this, withScope(uiTreeApiFacade.getParentScope(scope), arguments));
                            }
                        },
                        lastVisibleDescendant: function (scope, options) {
                            options.$event.preventDefault();

                            const lastChild = uiTreeApiFacade.getLastChildOf(scope);

                            if (lastChild) {
                                actions.smartNavigateTo.lastVisibleDescendant
                                    .apply(this, withScope(lastChild, arguments));
                            } else {
                                options.movePointerTo(scope);
                            }
                        },
                    },
                    navigateTo: {
                        previous: function navigateToPreviousNode(scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasPreviousSibling(scope)) {
                                options.movePointerTo(uiTreeApiFacade.getPreviousSiblingScope(scope));
                            }
                        },
                        next: function navigateToNextNodeOnLevel(scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasNextSibling(scope)) {
                                options.movePointerTo(uiTreeApiFacade.getNextSiblingScope(scope));
                            }
                        },
                        parent: function (scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasParentNode(scope)) {
                                options.movePointerTo(uiTreeApiFacade.getParentScope(scope));
                            }
                        },
                        child: function (scope, options) {
                            options.$event.preventDefault();

                            if (uiTreeApiFacade.hasChildNodes(scope)) {
                                options.movePointerTo(uiTreeApiFacade.getFirstChildOf(scope));
                            }
                        },
                    },
                    toggle: {
                        collapsed: function toggleVisibility(scope, options) {
                            options.$event.preventDefault();

                            uiTreeApiFacade.toggle(scope);
                        },
                    },
                };

                customActionsDefinitions.forEach(function (definition) {
                    Object.keys(definition).forEach(function (group) {
                        actions[group] = actions[group] || {};
                        Object.keys(definition[group]).forEach(function (action) {
                            actions[group][action] = definition[group][action];
                        });
                    });
                });

                return actions;
            };
        })
    /**
     * @ngdoc function
     * @name ui.tree-keyboard.factory:uiTreeKeyboardProxyActions
     *
     * @description
     * Allows to transform uiTreeKeyboardActions object into another one, having the same structure but with all actions
     * replaced by proxy function.
     *
     * Proxy function if the function what will call the original one and do something more or modify the original
     * arguments.
     */
        .factory('uiTreeKeyboardProxyActions', function uiTreeKeyboardProxyActions() {
            return function (actions, proxyFunction) {
                const proxyActions = {};
                Object.keys(actions).forEach(function (group) {
                    Object.keys(actions[group]).forEach(function (action) {
                        proxyActions[group] = proxyActions[group] || {};
                        proxyActions[group][action] = proxyFunction(actions[group][action]);
                    });
                });
                return proxyActions;
            };
        })
    /**
     * @ngdoc function
     * @name ui.tree-keyboard.factory:uiTreeKeyboardControls
     *
     * @description
     * @todo: description for uiTreeKeyboardControls
     *
     * @returns {{
     *      initialize: {Function},
     *      updatePointer: {Function},
     *      suppressShortcuts: {Function},
     *      enableShortcuts: {Function}
     * }}
     */
        .factory('uiTreeKeyboardControls',
        function uiTreeKeyboardControls($document, $timeout, keypressHelper, uiTreeApiFacade, uiTreeKeyboardActions,
                                        uiTreeKeyboardConfig) {
            /**
             * Pointer shared settings
             *
             * @type {{eventName: string, customMapping: {}, $scope: undefined}}
             */
            const pointerSettings = {
                eventName: '',
                customMapping: {},
                /** @private */
                $scope: undefined,
            };

            /**
             * Shortcut for binding the key map.
             *
             * If no keyMapOptions are provided key map from pointerSettings is used.
             *
             * @param {object} [keyMapOptions]
             * @param {boolean} keyMapOptions.useDefaultMapping
             * @param {object} keyMapOptions.customMapping
             */
            function bindMap(keyMapOptions) {
                keyMapOptions = keyMapOptions || {
                    customMapping: pointerSettings.customMapping,
                };
                keypressHelper(
                    pointerSettings.eventName,
                    pointerSettings.$scope,
                    $document,
                    null,
                    composeMap(uiTreeKeyboardActions, keyMapOptions)
                );
            }

            /**
             * Removes key bindings and optionally binds alternative set.
             *
             * It is meant toArray be used as a mean of suppressing the entire key map to enable other uses of
             * keystrokes for example for user input.
             *
             * @param {object} [temporaryMap]
             */
            function suppressShortcuts(temporaryMap) {
                $document.unbind(pointerSettings.eventName);

                if (temporaryMap) {
                    bindMap({
                        useDefaultMapping: false,
                        customMapping: temporaryMap,
                    });
                }
            }

            /**
             * Re-enables keyboard control with previous settings. Alternative scope can be provided.
             *
             * @param {object} [$scope]
             */
            function enableShortcuts($scope) {
                pointerSettings.$scope = $scope ? $scope : pointerSettings.$scope;
                bindMap();
            }

            /**
             * Initializes the module
             *
             * @param {object} [initialOptions]
             * @param {string} initialOptions.eventName
             * @param {object} initialOptions.customMapping
             */
            function initialize(initialOptions) {
                angular.extend(pointerSettings, initialOptions);
            }

            /**
             * Rebinds keyboard shortcuts to a new scope which then becomes a pointer.
             * In case scope of $type=uiTree is provided shortcuts are suppressed and pointer is removed.
             *
             * @param {object}   $scope any uiTreeNode scope to be stored as pointer or uiTree scope to remove pointer
             *                   entirely.
             * @param {boolean=} preventKeyboardBindingFlush There are some cases when one want to move the pointer
             *                   without refreshing the binding.
             */
            function updatePointer($scope, preventKeyboardBindingFlush) {
                preventKeyboardBindingFlush = preventKeyboardBindingFlush || false;

                if ($scope.$type === 'uiTree') {
                    $scope[uiTreeKeyboardConfig.pointerPropertyName] = null;
                    suppressShortcuts();
                    return;
                }
                $scope.$treeScope[uiTreeKeyboardConfig.pointerPropertyName] = $scope.$id;
                pointerSettings.$scope = $scope;

                // There are some rare cases when pointer is updated not directly by the user interaction (click or
                // cursor key hit) but as an effect of some other action like creating a node.
                if (preventKeyboardBindingFlush) {
                    return;
                }

                $timeout(function () {
                    suppressShortcuts();
                    enableShortcuts();
                });
            }

            /**
             * Amends a uiTreeKeyboard callback to provided ui.keypress 2nd argument (options)
             *
             * @param {Function} action
             * @returns {Function}
             *
             * @description
             * Wraps a function into another which recalls the given and adds the second argument (or only amends if it
             * already exists). The second argument is an object. It holds 1 or 2 properties:
             * `$event`        {Event} if the function was called by ui.keyboard
             * `movePointerTo` {scope} every time
             */
            function actionProxy(action) {
                return function () {
                    const args = [].slice.call(arguments);
                    args[1] = args[1] || {};
                    args[1].movePointerTo = updatePointer;
                    action.apply(this, args);
                };
            }

            /**
             * Prepares kay map using provided helper object.
             *
             * Key map is an object where each property name is an
             * {@link https://github.com/angular-ui/ui-utils/blob/master/modules/keypress/keypress.js ui.keypress}
             * expression and value is a callback function.
             *
             * @param {object} actions
             * @param {object} keyMapOptions
             * @param {boolean} keyMapOptions.useDefaultMapping
             * @param {object} keyMapOptions.customMapping
             *
             * @returns {object} key map
             */
            function composeMap(actions, keyMapOptions) {
                keyMapOptions.useDefaultMapping = 'useDefaultMapping' in keyMapOptions ?
                    keyMapOptions.useDefaultMapping :
                    true;
                keyMapOptions.customMapping = keyMapOptions.customMapping || {};

                const defaultMapping = {
                    up: actions.smartNavigateTo.previous,
                    down: actions.smartNavigateTo.next,
                    left: actions.smartNavigateTo.left,
                    right: actions.smartNavigateTo.right,
                    'shift-up': actions.navigateTo.previous,
                    'shift-down': actions.navigateTo.next,

                    'meta-up': actions.smartMove.up,
                    'meta-down': actions.smartMove.down,
                    'shift-meta-up': actions.move.up,
                    'shift-meta-down': actions.move.down,
                    'shift-alt-meta-up': actions.move.top,
                    'shift-alt-meta-down': actions.move.bottom,

                    'tab meta-right': actions.move.in,
                    'shift-tab meta-left': actions.move.out,

                    'meta-backspace meta-delete': actions.remove.confirmed,
                    'meta-alt-backspace meta-alt-delete': actions.remove.force,

                    'shift-78': actions.create.sibling, // "N".charAt(0) = 78
                    'shift-alt-78': actions.create.child, // "N".charAt(0) = 78

                    space: actions.toggle.collapsed,
                };

                const combinedMapping = {};
                const mappings = [];

                if (keyMapOptions.useDefaultMapping) {
                    mappings.push(defaultMapping);
                }

                if (Object.keys(keyMapOptions.customMapping).length) {
                    mappings.push(keyMapOptions.customMapping);
                }

                mappings.forEach(function (mapping) {
                    Object.keys(mapping).forEach(function (shortcutExpression) {
                        combinedMapping[shortcutExpression] = actionProxy(mapping[shortcutExpression]);
                    });
                });

                return combinedMapping;
            }

            return {
                initialize: initialize,
                updatePointer: updatePointer,
                actionProxy: actionProxy,
                suppressShortcuts: suppressShortcuts,
                enableShortcuts: enableShortcuts,
            };
        })
    /**
     * @ngdoc directive
     * @name ui.tree-keyboard.directive:uiTreeKeyboard
     *
     * @description
     * Initializes the module. The following options object can be provided:
     *
     * - `eventName`         {string}  name of the bound event. May (and if one use jQuery - should) be prefixed.
     * - `customMapping`     {object}  dictionary of shortcut actions. Keys maybe override the default ones.
     * - `setPointerOnFirst` {boolean} should the first rendered node be set as a pointer after the tree is initialised?
     *
     * When any of the settings is not provided a default stored in the uiTreeKeyboardConfig is used.
     */
        .directive('uiTreeKeyboard', function (uiTreeKeyboardControls, uiTreeKeyboardConfig) {
            return {
                restrict: 'A',
                require: 'uiTree',
                link: function (scope, elem, attrs) {
                    const initSettings =
                        scope[uiTreeKeyboardConfig.optionsPropertyName] = scope.$eval(attrs.uiTreeKeyboard) || {};

                    if (!initSettings.hasOwnProperty('setPointerOnFirst')) {
                        initSettings.setPointerOnFirst = uiTreeKeyboardConfig.setPointerOnFirst;
                    }

                    uiTreeKeyboardControls.initialize({
                        eventName: initSettings.eventName || uiTreeKeyboardConfig.eventName,
                        customMapping: initSettings.mapping || uiTreeKeyboardConfig.customMapping,
                    });
                },
            };
        })
    /**
     * @ngdoc directive
     * @name ui.tree-keyboard.directive:uiTreeKeyboardNode
     *
     * @description
     * Is responsible force setting the node elements class if pointerSettings is on the node.
     */
        .directive('uiTreeKeyboardNode', function (uiTreeKeyboardConfig, uiTreeKeyboardControls) {
            return {
                restrict: 'A',
                require: 'uiTreeNode',
                link: function (scope, elem) {
                    // Setting the first node as a pointer
                    if (scope[uiTreeKeyboardConfig.optionsPropertyName].setPointerOnFirst &&
                        scope.$parentNodesScope.$parent === scope.$treeScope && scope.$first) {
                        uiTreeKeyboardControls.updatePointer(scope);
                        elem.addClass('ui-tree-pointer');
                    }

                    scope.$watch(uiTreeKeyboardConfig.pointerPropertyName, function (newPointer, pointer) {
                        if (!angular.equals(newPointer, pointer)) {
                            if (scope.$id === pointer) {
                                elem.removeClass('ui-tree-pointer');
                            }
                            if (scope.$id === newPointer) {
                                elem.addClass('ui-tree-pointer');
                            }
                        }
                    });
                },
            };
        })
    /**
     * @ngdoc directive
     * @name ui.tree-keyboard.directive:uiTreeKeyboard
     *
     * @description
     * Allows to change pointerSettings to current on a given event.
     */
        .directive('uiTreeKeyboardSetPointerOn', function (uiTreeKeyboardControls) {
            return {
                restrict: 'A',
                controller: function uiTreeKeyboardSetPointerOnCtrl() {
                    this.eventName = '';
                },
                link: function (scope, elem, attrs, uiTreeKeyboardSetPointerOnCtrl) {
                    // pass the event name to uiTreeKeyboardSetPointerOff
                    uiTreeKeyboardSetPointerOnCtrl.eventName = attrs.uiTreeKeyboardSetPointerOn;
                    elem.on(attrs.uiTreeKeyboardSetPointerOn, function () {
                        scope.$apply(function () {
                            uiTreeKeyboardControls.updatePointer(scope);
                        });
                    });
                },
            };
        })
    /**
     * @ngdoc directive
     * @name ui.tree-keyboard.directive:uiTreeKeyboardSetPointerOff
     *
     * @description
     * Stops propagation of the event being watched by uiTreeKeyboardSetPointerOn
     */
        .directive('uiTreeKeyboardSetPointerOff', function ($timeout) {
            return {
                restrict: 'A',
                require: '^uiTreeKeyboardSetPointerOn',
                link: function (scope, elem, attrs, uiTreeKeyboardSetPointerOnCtrl) {
                    $timeout(function () {
                        elem.on(uiTreeKeyboardSetPointerOnCtrl.eventName, function (e) {
                            e.stopPropagation();
                        });
                    });
                },
            };
        })
    /**
     * @ngdoc directive
     * @name ui.tree-keyboard.directive:uiTreeKeyboardSuspend
     * @element input textarea
     *
     * @description
     * Provides easy way to temporarily disable tree keyboard shortcuts which enables full keyboard control over the
     * element.
     * May be used with ui.keypress to provide different set of shortcuts applied exclusively on the element both are
     * used on.
     */
        .directive('uiTreeKeyboardSuspend', function (uiTreeKeyboardControls) {
            return {
                restrict: 'A',
                link: function (scope, elem) {
                    elem
                        .on('focus', function () {
                            uiTreeKeyboardControls.suppressShortcuts();
                        })
                        .on('blur', function () {
                            uiTreeKeyboardControls.enableShortcuts();
                        });
                },
            };
        });
})(angular);
