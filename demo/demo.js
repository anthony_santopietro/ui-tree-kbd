(function (angular) {
    'use strict';

    angular.module('demoApp', [
        'ui.tree',
        'ui.tree-keyboard',
    ])
        .config(function(uiTreeApiFacadeProvider) {

            // Because Adding new nodes is tightly bound to the application logic it can be configured.
            uiTreeApiFacadeProvider.registerMethods({
                addChild: function (scope) {
                    var nodeData = scope.$modelValue;
                    var title = nodeData.title.split(' ');
                    nodeData.items.push({
                        id: nodeData.id * 10 + nodeData.items.length,
                        title: title[0] + (nodeData.items.length + 1) + '. ' + title[1] + '-child',
                        items: []
                    });
                },
                addSibling: function (scope) {
                    var items;
                    if (scope.$parentNodeScope) {
                        // 1+ level
                        items = scope.$parentNodeScope.$modelValue.items;
                    } else {
                        // level 0;
                        items = scope.$treeScope.$nodesScope.$modelValue;
                    }
                    items.push({
                        id: items.length + 1,
                        title: (items.length + 1) + '. New sibling item',
                        items: []
                    });
                },
            });
        })
        .controller('MainCtrl', function ($scope, uiTreeApiFacade) {
            $scope.filter = {
                pointerMove: true,
                nodeMove: true,
                nodeOps: true,
                smart: true,
            };

            $scope.list = [
                {
                    id: 1,
                    title: '1. dragon-breath',
                    items: []
                },
                {
                    id: 2,
                    title: '2. moiré-vision',
                    items: [
                        {
                            id: 21,
                            title: '2.1. tofu-animation',
                            items: [
                                {
                                    id: 211,
                                    title: '2.1.1. spooky-giraffe',
                                    items: []
                                },
                                {
                                    id: 212,
                                    title: '2.1.2. bubble-burst',
                                    items: []
                                }
                            ]
                        },
                        {
                            id: 22,
                            title: '2.2. barehand-atomsplitting',
                            items: []
                        }
                    ]
                },
                {
                    id: 3,
                    title: '3. unicorn-zapper',
                    items: []
                },
                {
                    id: 4,
                    title: '4. romantic-transclusion',
                    items: []
                }
            ];

            $scope.treeOptions = {};

            $scope.treeKeyboardOptions = {
                setPointerOnFirst: true,
            };

            $scope.remove = uiTreeApiFacade.remove;
            $scope.newSubItem = uiTreeApiFacade.addChild;
            $scope.newSibling = uiTreeApiFacade.addSibling;
        });
})(angular);