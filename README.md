#ui.tree-keyboard

A module providing an [AngularJS](http://angularjs.org/) directive using [angular-ui-tree](http://github.com/JimLiu/angular-ui-tree) and [ui-utils](http://github.com/angular-ui/ui-utils) to make the tree keyboard friendly.

## Quick start

```html
<ul ui-tree-keyboard ui-tree="treeOptions" ng-model="adjacencyTreeModel"></ul>
```
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Default setup](#default-setup)
	- [Tree navigation](#tree-navigation)
	- [Tree modification](#tree-modification)
- [Module architecture](#module-architecture)
	- [uiTreeKeyboard directive](#uitreekeyboard-directive)
	- [uiTreeKeyboardSetPointerOn directive](#uitreekeyboardsetpointeron-directive)
	- [uiTreeHelper provider](#uitreehelper-provider)
		- [uiTreeHelper API reference](#uitreehelper-api-reference)
			- [remove](#remove)
			- [addChild](#addchild)
			- [addSibling](#addsibling)
			- [toggle](#toggle)
			- [hasParentNode](#hasparentnode)
			- [hasChildNodes](#haschildnodes)
			- [isCollapsed](#iscollapsed)
			- [getParentScope](#getparentscope)
			- [getPreviousSiblingScope](#getprevioussiblingscope)
			- [getNextSiblingScope](#getnextsiblingscope)
		- [Modifying/extending helpers](#modifyingextending-helpers)
	- [uiTreeKeyboardActions provider](#uitreekeyboardactions-provider)
		- [uiTreeKeyboardActions API reference](#uitreekeyboardactions-api-reference)
			- [Create child](#create-child)
			- [Create sibling](#create-sibling)
			- [Remove confirmed](#remove-confirmed)
			- [Remove force](#remove-force)
			- [Move up](#move-up)
			- [Move down](#move-down)
			- [Move in (increase indentation)](#move-in-increase-indentation)
			- [Move out (decrease indentation)](#move-out-decrease-indentation)
			- [Move to the top](#move-to-the-top)
			- [Move to the bottom](#move-to-the-bottom)
			- [Smart navigate to left](#smart-navigate-to-left)
			- [Smart navigate to right](#smart-navigate-to-right)
			- [Smart navigate to previous](#smart-navigate-to-previous)
			- [Smart navigate to next](#smart-navigate-to-next)
			- [Smart navigate to nearest ancestor](#smart-navigate-to-nearest-ancestor)
			- [Smart navigate to last descendant](#smart-navigate-to-last-descendant)
			- [Navigate to previous](#navigate-to-previous)
			- [Navigate to next](#navigate-to-next)
			- [Navigate to parent](#navigate-to-parent)
			- [Navigate to child](#navigate-to-child)
			- [Toggle children visibility](#toggle-children-visibility)
		- [Modifying/extending actions](#modifyingextending-actions)
	- [uiTreeKeyboardControls factory](#uitreekeyboardcontrols-factory)
		- [uiTreeKeyboardControls API reference](#uitreekeyboardcontrols-api-reference)
	- [uiTreeKeyboardConfig value](#uitreekeyboardconfig-value)
- [Recipes](#recipes)
	- [Modifying the confirmation prompt text](#modifying-the-confirmation-prompt-text)
	- [Changing the mapped event](#changing-the-mapped-event)
	- [Changing the scope property holding pointer id](#changing-the-scope-property-holding-pointer-id)
	- [Using services in helper/action callback configuration](#using-services-in-helperaction-callback-configuration)
	- [Setting the pointer to the first tree element after the tree is rendered](#setting-the-pointer-to-the-first-tree-element-after-the-tree-is-rendered)
- [Contributing](#contributing)
- [Roadmap/wishlist](#roadmapwishlist)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Default setup

If no custom configuration is provided with directive settings or by using provider-based setup the following defaults
are available:

### Tree navigation

| Keyboard combination       | Action                                                            |
|----------------------------|-------------------------------------------------------------------|
| `up`                       | [smart navigate to previous](#smart-navigate-to-previous)         |
| `alt + up`                 | [navigate to previous](#navigate-to-previous)                     |
| `down`                     | [smart navigate to next](#smart-navigate-to-next)                 |
| `alt + down`               | [navigate to next](#navigate-to-next)                             |
| `left`                     | [smart navigate to left](#smart-to-left)                          |
| `right`                    | [smart navigate to right](#smart-to-right)                        |
| `space`                    | [toggle children visibility](#toggle-children-visibility)         |

### Tree modification

| Keyboard combination       | Action                                                            |
|----------------------------|-------------------------------------------------------------------|
| `ctrl/⌘ + up`              | [move up](#move-up)                                               |
| `alt+ ctrl/⌘ + up`         | [move to top](#move-to-the-top)                                   |
| `ctrl/⌘ + down`            | [move down](#move-down)                                           |
| `alt+ ctrl/⌘ + down`       | [move to bottom](#move-to-the-bottom)                             |
| `ctrl/⌘ + right`           | [move in (increase indentation)](#move-in-increase-indentation)   |
| `ctrl/⌘ + left`            | [move out (decrease indentation)](#move-out-decrease-indentation) |
| `tab`                      | [move in (increase indentation)](#move-in-increase-indentation)   |
| `shift + tab`              | [move out (decrease indentation)](#move-out-decrease-indentation) |
| `shift + N`                | [create sibling](#create-sibling)                                 |
| `shift + alt + N`          | [create child](#create-child)                                     |
| `ctrl/⌘ + backspace`       | [remove confirmed](#remove-confirmed)                             |
| `ctrl/⌘ + delete`          | [remove confirmed](#remove-confirmed)                             |
| `ctrl/⌘ + alt + delete`    | [remove force](#remove-force)                                     |
| `ctrl/⌘ + alt + backspace` | [remove force](#remove-force)                                     |

## Module architecture

Module contains of 6 parts:

- [uiTreeKeyboard directive](#uitreekeyboard-directive)
- [uiTreeKeyboardSetPointerOn directive](#uitreekeyboardsetpointeron-directive)
- [uiTreeHelper provider](#uitreehelper-provider)
- [uiTreeKeyboardActions provider](#uitreekeyboardactions-provider)
- [uiTreeKeyboardControls factory](#uitreekeyboardcontrols-factory)
- [uiTreeKeyboardConfig value](#uitreekeyboardconfig-value)


### uiTreeKeyboard directive

A directive which sets up the module. It should be placed at the root of the tree. This is frequently `<ol>` or `<ul>`
element. It requires the `uiNestedSortable` controller to make sure it's placed in the proper place.
It may be used without any settings or you may provide some custom configuration.

### uiTreeKeyboardSetPointerOn directive

Allows to bind an event listener which will set the pointer to directive scope to any tree node.
It can be used to set the pointer to clicked (or hovered/double clicked ets.) tree node.

### uiTreeHelper provider

A provider which returns an object that groups helper methods focusing on tree node operations like creating, deleting,
traversal, switching binary flags etc.

#### uiTreeHelper API reference

The default structure is:

```javascript
{
    remove: function,
    addChild: function,
    addSibling: function,
    toggle: function,
    hasParentNode: function,
    hasChildNodes: function,
    isCollapsed: function,
    getParentScope: function,
    getPreviousSiblingScope: function,
    getNextSiblingScope: function
}
```

##### remove

```javascript
uiTreeHelper.remove(scope);
```
Removes a scope and its children.

##### addChild
```javascript
uiTreeHelper.addChild(scope);
```
Creates a child inside current scope.

##### addSibling
```javascript
uiTreeHelper.addSibling(scope);
```
Creates a child next to current scope.

##### toggle
```javascript
uiTreeHelper.toggle(scope);
```
Changes child nodes visibility flag.

##### hasParentNode
```javascript
uiTreeHelper.hasParentNode(scope);
```
Returns `true` if the node connected with scope has a parent.

##### hasChildNodes
```javascript
uiTreeHelper.hasChildNodes(scope);
```
Returns `true` if the node connected with scope has a children.

##### isCollapsed
```javascript
uiTreeHelper.isCollapsed(scope);
```
Returns `true` if the node connected with scope has a children and collapsed flag is true.

##### getParentScope
```javascript
uiTreeHelper.getParentScope(scope);
```
Returns the scope of parent node;

##### getPreviousSiblingScope
```javascript
uiTreeHelper.getPreviousSiblingScope(scope);
```
Returns the scope of the previous sibling if it exists.

##### getNextSiblingScope
```javascript
uiTreeHelper.getNextSiblingScope(scope);
```
Returns the scope of the next sibling if it exists.

#### Modifying/extending helpers

In `config` block of any module requiring `ui.tree-keyboard` you can use `registerHelpers` to provide an object that
will be merged with the [default object structure](#uiTreeHelper-API-reference).

Not only you can override the existing methods but you can also create your own, which will be later utilised by
[default actions](#uiTreeKeyboardActions-API-reference) or custom actions.

```javascript
uiTreeHelperProvider.registerHelpers({
    isCollapsed: yourOwnImplementationOfIsCollapsed,
})
```

### uiTreeKeyboardActions provider

A provider which returns an object with functions performing tree navigation or tree modification. One can also
[add their own actions or modify the existing ones](#modifyingextending-actions).

#### uiTreeKeyboardActions API reference

The default structure is:

```javascript
{
    create: {
        child: function,
        sibling: function,
    },
    remove: {
        confirmed: function,
        force: force,
    },
    move: {
        up: function,
        down: function,
        in: function,
        out: function,
        top: function,
        bottom: function,
    },
    smartNavigateTo: {
        left: function,
        right: function,
        previous: function,
        next: function,
        nearestAncestor: function,
        lastDescendant: function
    },
    navigateTo: {
        previous: function,
        next: function,
        parent: function,
        child: function,
    },
    toggle: {
        visibility: function
    }
}
```

##### Create child

```javascript
uiTreeKeyboardActions.create.child(scope, options);
```
Creates a new tree node inside the current one.
The pointer is not transferred to the created node.

##### Create sibling

```javascript
uiTreeKeyboardActions.create.sibling(scope, options);
```

Creates a new tree node after the current one.
The pointer is not transferred to the created node.

##### Remove confirmed

```javascript
uiTreeKeyboardActions.remove.confirmed(scope, options);
```

Removes the current node. If the node has child nodes user shall be ashed for confirmation. After the user confirmed the action controll is passed to [remove force](#remove-force) action.
The pointer is transferred to other node according to the following rule:

1. transfer the pointer to the prvious sibling if previous sibling exists
2. transfer the pointer to parent if parent exists
3. transfer the pointer to next sibling
4. remove the pointer

The latter happens only after the last node has been removed.

_NOTE_: you can customize the confirmation prompt text. See [Modifying the confirmation prompt text](#modifying-the-confirmation-prompt-text) in [Recepies](#recepies).

##### Remove force

```javascript
uiTreeKeyboardActions.remove.force(scope, options);
```
Removes the current node without user confirmation.

##### Move up

```javascript
uiTreeKeyboardActions.move.up(scope, options);
```
_TODO_: implement this feature.

Switches the node with its previous sibling. Preserves child nodes.

##### Move down

```javascript
uiTreeKeyboardActions.move.up(scope, options);
```
_TODO_: implement this feature.

Switches the node with its next sibling. Preserves child nodes.

##### Move in (increase indentation)

```javascript
uiTreeKeyboardActions.move.in(scope, options);
```
_TODO_: implement this feature.

Makes the node its previous sibling descendant.
Does nothing if the node is a first child of its ancestor.

##### Move out (decrease indentation)

```javascript
uiTreeKeyboardActions.move.out(scope, options);
```
_TODO_: implement this feature.

Makes the node its parent sibling.
Does nothing if the node does not have a parent.

##### Move to the top

```javascript
uiTreeKeyboardActions.move.top(scope, options);
```
_TODO_: implement this feature.

Makes the node a first child of its parent or the forst node if it's placed on level 0.
Does nothing if the node already is a first child.

##### Move to the bottom

```javascript
uiTreeKeyboardActions.move.bottom(scope, options);
```
_TODO_: implement this feature.

Makes the node a last child of its parent or the last node if it's placed on level 0.
Does nothing if the node already is a last child.

##### Smart navigate to left

```javascript
uiTreeKeyboardActions.smartNavigateTo.left(scope, options);
```

Moves pointer to nodes first child or expands nodes children if they were collapsed.

##### Smart navigate to right

```javascript
uiTreeKeyboardActions.smartNavigateTo.right(scope, options);
```

Moves pointer to nodes parent or collapses nodes children if they were expanded.

##### Smart navigate to previous

```javascript
uiTreeKeyboardActions.smartNavigateTo.previous(scope, options);
```
Moves the pointer to a node that is visually above the current one.

It is achieved according to the following rules:

- Move the pointer to previous sibling if it exists, has no children or if it's collapsed (children are hidden)
- Move the pointer to parent if node is its parent first child and parent exists.
- Move the pointer to previous siblings [last descendant](#smart-navigate-to-last-descendant)

##### Smart navigate to next

```javascript
uiTreeKeyboardActions.smartNavigateTo.next(scope, options);
```
Moves the pointer to a node that is visually below the current one.

It is achieved according to the following rules:

- Move the pointer to [nearest ancestor next sibling](#smart-navigate-to-nearest-ancestor-next-sibling)
- Move the pointer to first child if the node has children
- Move the pointer to next sibling if it exists

##### Smart navigate to nearest ancestor

```javascript
uiTreeKeyboardActions.smartNavigateTo.nearestAncestorNextSibling(scope, options);
```

Recursively finds first ancestor that has a next sibling and returns that sibling.

##### Smart navigate to last descendant

```javascript
uiTreeKeyboardActions.smartNavigateTo.lastDescendant(scope, options);
```

Recursively finds the last childs last child until a leaf is reached.
_NOTE_: this si not supposed to find a leaf on the highest level.

##### Navigate to previous

```javascript
uiTreeKeyboardActions.navigateTo.previous(scope, options);
```
Moves the pointer to previous sibling or does nothing if a node is first on level.

##### Navigate to next

```javascript
uiTreeKeyboardActions.navigateTo.next(scope, options);
```
Moves the pointer to the next sibling or does nothing if a node is last on level.

##### Navigate to parent

```javascript
uiTreeKeyboardActions.navigateTo.parent(scope, options);
```
Moves the pointer to the parent if it exists.

##### Navigate to child

```javascript
uiTreeKeyboardActions.navigateTo.child(scope, options);
```
Moves the pointer to the first child if it exists.

##### Toggle children visibility

```javascript
uiTreeKeyboardActions.toggle.visibility(scope, options);
```
Switches visibility of nodes children.

#### Modifying/extending actions

In `config` block of any module requiring `ui.tree-keyboard` you can use `registerActions` to provide an object that
will be merged with the [default object structure](#uiTreeKeyboardActions-API-reference).

Not only you can override the existing methods but you can also create your own, which will be later utilised in
[directive configuration](#uiTreeKeyboard-directive) in custom key map.

```javascript
uiTreeKeyboardActionsProvider.registerActions({
    smartNavigateTo: {
        left: yourOwnImplementationOfTheMethod
    }
    yourOwnMethodOrMethodGroup: ...
})
```

### uiTreeKeyboardControls factory

#### uiTreeKeyboardControls API reference

The default structure is:

```javascript
{
    updatePointer: function,
    suppressShortcuts: function,
    enableShortcuts: function,
}
```

### uiTreeKeyboardConfig value

Can be used to customize the module globally.

## Recipes

ui.tree-keyboard module is designed to allow you as much extensibility as possible.

### Modifying the confirmation prompt text

You can modify the text by setting up the `uiTreeKeyboardConfig` value property.

```javascript
uiTreeKeyboardConfig.confirmationPromptText = 'Your own text for confirmation';
```

This setting will be the new default for all trees you use in your entire project. You can also implement you own logic
to provide l10n;

### Changing the mapped event

If you are not comfortable with the default `keyup` event you can change it by calling


```javascript
uiTreeKeyboardConfig.eventName = 'keydown';
```

All events supported by [ui-utils ui.keypress](https://github.com/angular-ui/ui-utils/tree/master/modules/keypress) are
available.

__REMEMBER__: It is highly recommended to namespace your events:

```javascript
uiTreeKeyboardConfig.eventName = 'keydown.uiTreeKeyboard';
```

### Changing the scope property holding pointer id

ui.tree-keyboard benefits from Javascript prototypical inheritance and uses a property on tree root level scope to hold
the id of current pointer. By default this property is called `uiTreeKeyboard` but you can change it as well in 2 ways:

- by changing `uiTreeKeyboardConfig` property in which case the name will be used for all trees.
```javascript
uiTreeKeyboardConfig.statePropertyName = 'yourOwnPropertyName';
```

- by passing the property name in directive config in which case the name will only be used for this single tree.
```html
<ul ui-tree-keyboard="{statePropertyName: 'yourOwnPropertyName'}"
    ui-tree="treeOptions"
    ng-model="adjacencyTreeModel"></ul>
```

### Using services in helper/action callback configuration

If you wish for helpers or actions to perform tasks with extended logic it's convenient to wrap them into services.
Though `ui.keyboard-tree` does not provide any convenient way to pass a service returning an object with proper methods
you can do it yourself by using `angular.injector().invoke()`.

```javascript
uiTreeKeyboardActionsProvider.registerActions({
    remove: {
        confirmed: function removeNodeConfirmed() {
            var argsArray = [].slice.call(arguments);
            angular.injector([
                'ng',
                'ui.bootstrap',
                'projectRemovalConfirmation',
                'ui.tree-keyboard'
            ]).invoke([
                'projectRemovalConfirmation',
                'uiTreeKeyboardActions',
                function (projectRemovalConfirmation, uiTreeKeyboardActions) {
                    projectRemovalConfirmation(uiTreeKeyboardActions.remove.force, argsArray);
                }
            ]);
        }
    }
});
```

_NOTE_: When you use `injector` you have to explicitly provide all the modules. Dependencies do not seem to be resolved.

```javascript
angular.module('projectRemovalConfirmation', [
        'ui.bootstrap',
        'ui.tree-keyboard',
     ])
    .factory('projectRemovalConfirmation', function($modal, uiTreeHelper) {
        var template = '' +
            '<div class="modal-header">' +
            '    <h3>Confirm element removal</h3>' +
            '</div>' +
            '<div class="modal-body">' +
            '    <p>Item you\'re about to remove contains sub-elements.' +
            '       Do you really want to remove them?</p>' +
            '</div>' +
            '<div class="modal-footer">' +
            '    <button class="btn btn-warning" ng-click="$close()">OK, remove</button>' +
            '    <button class="btn btn-default" ng-click="$dismiss()">No, stop</button>' +
            '</div>';

        return function (callback, argsArray) {
            var $scope = argsArray[0];
            var rest = argsArray.slice(1);
            // Business logic: ask for confirmation using $modal if node has child nodes.
            if (uiTreeHelper.hasChildNodes(scope)) {
                $modal
                    .open({
                        template: template,
                    })
                    .result
                    .then(function () {
                        callback.apply(this, [$scope].concat(rest));
                    });
            } else {
                callback.apply(this, [$scope].concat(rest));
            }
        }
    });
```

`projectRemovalConfirmation` can takes a callback (in this case `uiTreeKeyboardActions.remove.force`) to run
it with arguments passed to a function when user confirms the node removal. The only difference is that the prompt is
configurable using `$modal` service

___WARNING___: you should be extremely cautious using this way of injecting services. You should not make your module or
application call it during the config phase. You can though create callbacks using it - just like described above.

### Setting the pointer to the first tree element after the tree is rendered

## Contributing

## Roadmap/wishlist

* Support for moving tree nodes
* Test suite