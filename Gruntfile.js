module.exports = function (grunt) {
    'use strict';

    // load all grunt tasks automatically and only necessary ones.
    // Those that doesn't match the pattern have to be provided here.
    require('jit-grunt')(grunt, {
        jscs: 'grunt-jscs-checker',
    });

    var mountFolder = function (connect, dir) {
        return connect.static(require('path').resolve(dir));
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        defs: {
            options: {
                transformDest: function transformDest(path) {
                    return path.replace(/\.js$/, '.defs.js');
                },
                defsOptions: {
                    disallowDuplicated: true,
                    disallowUnknownReferences: false,
                    disallowVars: true,
                },
            },
            all: {
                src: [
                    'src/<%= pkg.name %>.js',
                ],
            },
        },
        ngAnnotate: {
            demo: {
                files: {
                    '.tmp/<%= pkg.name %>.js': ['src/<%= pkg.name %>.defs.js'],
                },
            },
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= pkg.version %>, <%= grunt.template.today("dd-mm-yyyy") %> */\n',
            },
            build: {
                src: '.tmp/<%= pkg.name %>.js',
                dest: 'dist/<%= pkg.name %>.min.js',
            },
        },
        copy: {
            demo: {
                src: 'dist/<%= pkg.name %>.min.js',
                dest: 'demo/',
            },
            dev: {
                src: '.tmp/<%= pkg.name %>.js',
                dest: 'dist/<%= pkg.name %>.js',
            },
        },
        clean: {
            tmp: '.tmp',
        },
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                hostname: '0.0.0.0',
                open: true,
            },
            demo: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, 'demo'),
                        ];
                    },
                },
            },
        },
        watch: {
            livereload: {
                files: [
                    'src/<%= pkg.name %>.js',
                    'demo/*.js',
                    'demo/*.css',
                    'demo/*.html',
                    '!demo/bower_components/**/*',
                ],
                options: {
                    livereload: true,
                },
                tasks: ['build'],
            },
        },
        jshint: {
            options: {
                jshintrc: true,
            },
            all: {
                src: [
                    'Gruntfile.js',
                    'src/<%= pkg.name %>.js',
                ],
            },
        },
        jscs: {
            all: {
                src: '<%= jshint.all.src %>',
                options: {
                    config: '.jscs.json',
                },
            },
        },
        'merge-conflict': {
            files: '<%= jshint.all.src %>',
        },
    });


    grunt.registerTask('lint', [
        'jshint',
        'jscs',
        'merge-conflict',
    ]);

    grunt.registerTask('build', [
        'lint',
        'defs',
        'ngAnnotate',
        'copy:dev',
        'uglify',
        'copy:demo',
        'clean:tmp',
    ]);

    grunt.registerTask('demo', [
        'build',
        'connect:demo',
        'watch',
    ]);

    grunt.registerTask('serve', [
        'build',
        'watch',
    ]);

    grunt.registerTask('default', [
        'serve',
    ]);
};
